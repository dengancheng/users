module users

go 1.14

require (
	framework v0.0.0
	github.com/asmcos/requests v0.0.0-20181227105501-5384de75ad0b
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575
	github.com/juju/errors v0.0.0-20181118221551-089d3ea4e4d5
	github.com/kataras/iris v11.1.1+incompatible
	go.mongodb.org/mongo-driver v1.3.3
)

replace framework => ../framework
