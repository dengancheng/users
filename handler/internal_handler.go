package handler

import (
	"framework/helper/mongoHelper"
	"go.mongodb.org/mongo-driver/bson"
	"users/defines"
)

func GenerateAccessToken() string {
	return defines.PREFIX_ACCESS_TOKEN+GenerateObjectIdHex()
}

func GetDepartmentDetail() (*[]defines.DepartmentInfo,error) {
	context := mongoHelper.MongoContext()
	cursor, err := mongoHelper.MongoDb.Collection("department").Find(nil, bson.D{})
	if err != nil {
		return nil,err
	}
	defer cursor.Close(nil)

	var dInfoList []defines.DepartmentInfo
	for cursor.Next(context) {
		var dInfo defines.DepartmentInfo
		err := cursor.Decode(&dInfo)
		if err != nil {
			return nil,err
		} else {
			dInfoList = append(dInfoList, dInfo)
		}
	}

	return &dInfoList,nil
}
