package handler

import (
	"framework/helper/mongoHelper"
	"framework/helper/redisHelper"
	log "github.com/cihub/seelog"
	"github.com/kataras/iris"
	"go.mongodb.org/mongo-driver/bson"
	"users/defines"
)

func RegisterHandler(ctx iris.Context) {
	log.Debug("RegisterHandler")

	var registerReq defines.Register
	err := ctx.ReadJSON(&registerReq)
	if err != nil {
		log.Error(err.Error())
		ctx.StatusCode(iris.StatusBadRequest)
		return
	}

	//store into mongo
	eId := GenerateObjectIdHex()
	if registerReq.EmployeeID != "" {
		eId = registerReq.EmployeeID
	}

	err = UpsertDocument("register", "_id", eId, registerReq)
	if err != nil {
		log.Error(err.Error())
		ctx.StatusCode(iris.StatusInternalServerError)
		return
	}

	//更新员工部门
	for _,dpId := range registerReq.Departments {
		//先读取
		dpInfo, err := GetDepartmentInfoByDpid(dpId)
		if err != nil {
			log.Error(err.Error())
			ctx.StatusCode(iris.StatusInternalServerError)
			return
		}

		if dpInfo == nil {
			log.Errorf("Cannot find info of dpid:%s", dpId)
			continue
		}

		found := false
		if dpInfo.EmployeeIds == nil {
			dpInfo.EmployeeIds = make([]string,0)
		}
		for _,v := range dpInfo.EmployeeIds {
			if v == registerReq.EmployeeID {
				found = true
				break
			}
		}

		if found == false {
			dpInfo.EmployeeIds = append(dpInfo.EmployeeIds, registerReq.EmployeeID)
			err := UpsertDocument("department", "_id", dpInfo.Dpid, dpInfo)
			if err != nil {
				log.Error(err.Error())
				ctx.StatusCode(iris.StatusInternalServerError)
				return
			}
		}

	}

	log.Debugf("User %s has registered successfully", registerReq.ID)
	ctx.StatusCode(iris.StatusOK)
	return
}

func LoginHttpHandler(ctx iris.Context) {

	//校验用户名和密码
	id := ctx.GetHeader("id")
	loginPwd := ctx.GetHeader("pwd")
	if id == "" || loginPwd == "" {
		log.Error("Login id or pwd are empty")
		ctx.StatusCode(iris.StatusBadRequest)
		return
	}

	userInfoMap, err := GetUserInfoByX("register", "id", id)
	if err != nil {
		log.Error(err.Error())
		ctx.StatusCode(iris.StatusForbidden)
		return
	}

	realPwd,ok := userInfoMap["password"]
	if ok {
		if loginPwd != realPwd {
			//密码错误
			log.Error("Wrong password")
			ctx.StatusCode(iris.StatusForbidden)
			return
		}
	}

	eId, ok := userInfoMap["employee_id"].(string)
	if !ok {
		log.Error("Cannot find employee_id in mongo")
		ctx.StatusCode(iris.StatusInternalServerError)
		return
	}

	//generate access_token
	accessToken := GenerateAccessToken()
	if accessToken=="" {
		log.Error("Faield to generate access token")
		ctx.StatusCode(iris.StatusInternalServerError)
		return
	}

	redisHelper.SetWithExpireTransaction(accessToken,
		eId,
		defines.REDIS_USER_CACHE_EXPIRE)

	//返回用户数据(包含access_token)
	var userInfo defines.UserInfo
	userInfo.Uid = eId
	userInfo.LeaderUid,ok = userInfoMap["leader"].(string)
	userInfo.Name,ok = userInfoMap["name"].(string)
	userInfo.Email,ok = userInfoMap["email"].(string)
	userInfo.AvatarUrl,ok = userInfoMap["avatar_url"].(string)
	userInfo.AccessToken = accessToken
	userInfo.ExpiresIn = defines.REDIS_USER_CACHE_EXPIRE

	ctx.JSON(userInfo)
	ctx.StatusCode(iris.StatusOK)
	return
}

func GetUserInfoByUidHandler(ctx iris.Context) {
	userId := ctx.GetHeader("id")
	if userId == "" {
		ctx.StatusCode(iris.StatusBadRequest)
		log.Error("UserId is empty")
		return
	}

	//Load userInfo from mongo by employee_id or _id
	userInfoMap, err := GetUserInfoByX("register","_id", userId)
	if err != nil {
		log.Error(err.Error())
		ctx.StatusCode(iris.StatusInternalServerError)
		return
	}

	ctx.JSON(userInfoMap)
	ctx.StatusCode(iris.StatusOK)

	return
}

func GetUserInfoHandler(ctx iris.Context) {
	accessToken := ctx.GetHeader("token")
	if accessToken == "" {
		log.Error("AccessToken is empty")
		ctx.StatusCode(iris.StatusBadRequest)
		return
	}
	log.Debug("GetUserInfoHandler, access_token:", accessToken)

	eId, err := redisHelper.Get(defines.PREFIX_ACCESS_TOKEN+accessToken)
	if err != nil {
		//access_token过期，需要重新登录
		log.Error(err.Error())
		ctx.StatusCode(iris.StatusForbidden)
		return
	}

	//Load userInfo from mongo by employee_id or _id
	userInfoMap, err := GetUserInfoByX("register","_id", eId)
	if err != nil {
		log.Error(err.Error())
		ctx.StatusCode(iris.StatusInternalServerError)
		return
	}

	ctx.JSON(userInfoMap)
	ctx.StatusCode(iris.StatusOK)

	return
}

func CreateDepartmentHandler(ctx iris.Context) {
	log.Debug("CreateDepartmentHandler")

	var dptInfo defines.DepartmentInfo
	err := ctx.ReadJSON(&dptInfo)
	if err != nil {
		log.Error(err.Error())
		ctx.StatusCode(iris.StatusBadRequest)
		return
	}

	dpid := GenerateObjectIdHex()
	if dptInfo.Dpid != "" {
		dpid = dptInfo.Dpid
	}

	//store into mongo
	err = UpsertDocument("department", "_id", dpid, dptInfo)
	if err != nil {
		log.Error(err.Error())
		ctx.StatusCode(iris.StatusInternalServerError)
		return
	}

	//TODO:redis cache
}

func ListDepartmentHandler(ctx iris.Context) {
	log.Debug("ListDepartmentHandler")

	dInfoList, err := GetDepartmentDetail()
	if err != nil {
		log.Error(err.Error())
		ctx.StatusCode(iris.StatusInternalServerError)
		return
	}

	ctx.JSON(*dInfoList)

	//TODO:redis cache
}

func GetUndersHandler(ctx iris.Context) {
	leaderEid := ctx.GetHeader("id")
	if leaderEid == "" {
		ctx.StatusCode(iris.StatusBadRequest)
		log.Error("GetUndersHandler, Id is empty")
		return
	}

	context := mongoHelper.MongoContext()
	cursor, err := mongoHelper.MongoDb.Collection("register").Find(nil, bson.M{"leader":leaderEid})
	if err != nil {
		log.Error(err.Error())
	}
	defer cursor.Close(nil)

	var underInfoList []defines.UnderInfo
	for cursor.Next(context) {
		var userInfo defines.Register
		err := cursor.Decode(&userInfo)
		if err != nil {
			log.Error(err.Error())
			ctx.StatusCode(iris.StatusInternalServerError)
			return
		}
		currentUnder := defines.UnderInfo{Uid:userInfo.EmployeeID, Name:userInfo.Name, AvatarUrl:userInfo.AvatarUrl}
		underInfoList = append(underInfoList, currentUnder)
	}

	ctx.JSON(underInfoList)
    ctx.StatusCode(iris.StatusOK)
	return
}

func RefreshTokenHandler(ctx iris.Context) {
	accessToken := ctx.GetHeader("token")
	log.Debug("RefreshTokenHandler, accessToken:", accessToken)
	if accessToken == "" {
		log.Error("AccessToken is empty")
		ctx.StatusCode(iris.StatusBadRequest)
		return
	}

	ctx.StatusCode(iris.StatusForbidden)
	return
}

func GetContactAllUsers(ctx iris.Context) {
	accessToken := ctx.GetHeader("token")
	log.Infof("GetContactAllUsers token:%s", accessToken)

	dpList,err := GetDepartmentDetail()
	if err != nil {
		log.Error(err.Error())
		ctx.StatusCode(iris.StatusInternalServerError)
		return
	}

	var depContactList []defines.ContactData
	for _, dpInfo := range *dpList {
		var depContact defines.ContactData
		depContact.DepId = dpInfo.Dpid
		depContact.DepName = dpInfo.Name
		for _,eid := range dpInfo.EmployeeIds {
			log.Debugf("DpName:%s, eId:%s", dpInfo.Name, eid)
			userInfoMap, err := GetUserInfoByX("register", "employee_id", eid)
			if err != nil {
				log.Error(err.Error())
				continue
			}
			if userInfoMap == nil {
				log.Errorf("Illegal employeeId:%s", eid)
				continue
			}
			var userInfo defines.UserInfo
			userInfo.Name,_ = userInfoMap["name"].(string)
			userInfo.Uid,_ = userInfoMap["employee_id"].(string)
			userInfo.LeaderUid,_ = userInfoMap["leader"].(string)
			userInfo.AvatarUrl,_ = userInfoMap["avatar_url"].(string)
			depContact.UserList = append(depContact.UserList, userInfo)
		}
		depContactList = append(depContactList, depContact)
	}

	ctx.JSON(depContactList)
	return
}
