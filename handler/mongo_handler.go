package handler

import (
	"framework/helper/mongoHelper"
	log "github.com/cihub/seelog"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"users/defines"
)

func GenerateObjectIdHex() string {
	objId := primitive.NewObjectID()
	return objId.Hex()
}

func UpsertDocument(collectionName string, key string, keyValue string, doc interface{}) error {
	log.Debugf("UpsertDocument collectionName:%s, key:%s, keyValue:%s", collectionName, key, keyValue)

	res, err := mongoHelper.MongoDb.Collection(collectionName).UpdateOne(nil,
		bson.M{key: keyValue},
		bson.D{{"$set", doc}},
		options.Update().SetUpsert(true))
	if err != nil {
		log.Error("Failed to upsert one:", err.Error())
		return err
	}
	upsertId := res.UpsertedID
	log.Debug("Upsert into mongo succeeded:", upsertId)
	return nil
}

func GetUserInfoByX(collectionName string, keyName string, keyValue string) (map[string]interface{}, error) {
	uinfoData := make(map[string]interface{})
	err := mongoHelper.MongoDb.Collection(collectionName).FindOne(nil, bson.M{keyName: keyValue}).Decode(&uinfoData)
	log.Debugf("GetUserInfoBy %s:%s, %v", keyName, keyValue, uinfoData)
	if err != nil {
		if err.Error() == mongo.ErrNoDocuments.Error() {
			log.Debugf("Cannot find user info of user whose %s is %s", keyName, keyValue)
			return nil,nil
		} else {
			log.Errorf("Failed to read user info from mongo, %s:%s, err:%s", keyName, keyValue, err.Error())
			return nil,err
		}
	} else {
		uinfoData["uid"] = uinfoData["_id"]
		return uinfoData,nil
	}
}

func GetDepartmentInfoByDpid(keyValue string) (*defines.DepartmentInfo, error) {
	var dpDetail defines.DepartmentInfo
	//dpObjId,_ := primitive.ObjectIDFromHex(keyValue)
	err := mongoHelper.MongoDb.Collection("department").FindOne(nil, bson.M{"dpid":keyValue}).Decode(&dpDetail)
	log.Debugf("GetDepartmentInfoByDpid %s, %v", keyValue, dpDetail)
	if err != nil {
		if err.Error() == mongo.ErrNoDocuments.Error() {
			return nil,nil
		} else {
			return nil,err
		}
	} else {
		return &dpDetail,nil
	}
}
