package http

import (
	"github.com/kataras/iris"
	"users/handler"
)

func HttpService(app *iris.Application) {

	//RESTFUL API
	app.Post("register", handler.RegisterHandler)
	app.Get("user_login", handler.LoginHttpHandler)
	app.Get("user_token_info", handler.GetUserInfoHandler)
	app.Get("user_id_info", handler.GetUserInfoByUidHandler)
	app.Post("department/create", handler.CreateDepartmentHandler)
	app.Get("department/list", handler.ListDepartmentHandler)
	app.Get("unders", handler.GetUndersHandler)
	//app.Get("refresh_token", handler.RefreshTokenHandler)
	app.Get("get_contact_all_users", handler.GetContactAllUsers)

}
