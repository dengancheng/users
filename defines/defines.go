package defines

var REDIS_PREFIX_USER_ID_PWD = "USER_ID_PWD_"
var REDIS_PREFIX_USER_ID_NAME = "USER_ID_NAME_"
const REDIS_USER_CACHE_EXPIRE = 3600*24*30

var PREFIX_ACCESS_TOKEN = "UTK_"

type Register struct {
	ID string `json:"id" bson:"id"`
	Name string `json:"name" bson:"name"`
	Phone string `json:"phone,omitempty" bson:"phone,omitempty"`
	Password string `json:"password" bson:"password"`
	Gender int `json:"gender" bson:"gender"`  //0:女 1:男
	Departments []string `json:"departments" bson:"departments"`  //部门
	Leader string `json:"leader,omitempty" bson:"leader,omitempty"`  //上级
	Level int `json:"level,omitempty bson:"level,omitempty"`  //权限管理, 1:正式员工，2:临时工
	EmployeeID string `json:"employee_id,omitempty" bson:"employee_id,omitempty"`  //指定emplyee_id
	AvatarUrl string `json:"avatar_url,omitempty" bson:"avatar_url,omitempty"`
}

type UserInfo struct {
	AccessToken string `json:"access_token,omitempty" bson:"access_token,omitempty"`
	AvatarUrl string `json:"avatar_url,omitempty" bson:"avatar_url,omitempty"`
	Name string `json:"name,omitempty" bson:"name,omitempty"`
	EnName string `json:"en_name,omitempty" bson:"en_name,omitempty"`
	ExpiresIn float64 `json:"expires_in,omitempty" bson:"expires_in,omitempty"`
	LeaderUid string `json:"leader_uid,omitempty" json:"leader_uid,omitempty"`
	UnderUids []string `json:"under_uids,omitempty" bson:"under_uids,omitempty"`
	Email string `json:"email,omitempty" bson:"email,omitempty"`
	Uid string `json:"uid,omitempty" bson:"uid,omitempty"`
	//OpenId string `json:"open_id,omitempty" bson:"open_id,omitempty"`
	Status int `json:"status,omitempty" bson:"status,omitempty"`
}

type DepartmentInfo struct {
	Name  string `json:"name,omitempty" bson:"name,omitempty"`
	Dpid  string `json:"dpid,omitempty" bson:"dpid,omitempty"`
	EmployeeIds []string `json:"employee_ids,omitempty" bson:"employee_ids,omitempty"`
}

type ContactData struct {
	DepId  string `json:"depid,omitempty"`
	DepName string `json:"depname,omitempty"`
	UserList []UserInfo `json:"user_list,omitempty"`
}

type UnderInfo struct {
	Uid string `json:"uid"`
	Name string `json:"name"`
	AvatarUrl string `json:"avatar_url"`
}
