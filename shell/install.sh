#!/bin/bash

project=$1
workspace=`pwd`/workspace
supervisord_config=/etc/supervisord.d/$project.ini

echo $project
echo $workspace
echo $supervisord_config

if [ ! -d "$workspace" ]; then
  mkdir $workspace
fi

cp -rf tmp/output/main $workspace/$project
cp -rf tmp/output/config/ $workspace/

if [ ! -f "$supervisord_config" ]; then
  echo "create $supervisord_config and supervisorctl update"
  echo "[program:$project]" > $supervisord_config
  echo "command=$workspace/$project -w $workspace" >> $supervisord_config
  echo "user=root" >> $supervisord_config
  echo "autostart=true" >> $supervisord_config
  echo "autorestart=true" >> $supervisord_config
  echo "startsecs=5" >> $supervisord_config
  echo "stopasgroup=true" >> $supervisord_config
  echo "killasgroup=true" >> $supervisord_config
  echo "startretries=3" >> $supervisord_config
  echo "directory=$workspace" >> $supervisord_config
  supervisorctl update
else 
  echo "supervisorctl restart $project"
  supervisorctl restart $project
fi

echo "waiting 3s ..."
sleep 3s
supervisorctl status

echo "======= 读取运行日志最后50行 ========"
tail -n 50 workspace/logs/server.log

