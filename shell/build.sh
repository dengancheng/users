#!/bin/bash

environment=$1

export GO111MODULE=on
export GOPROXY=https://mirrors.aliyun.com/goproxy/

if [ "$environment" != "release" ]; then
  environment="develop"
fi

cd ..

echo "==== 开始编译： 环境: $environment ==== "
echo "==== 重新创建output ===="
rm -rf output
rm -rf output.zip
mkdir output
mkdir output/config

echo "==== 编译生成可执行文件main ==== "
go build -o main

if [ "$?" != "0" ]; then
  exit $?
fi

echo "==== Copy重要文件到output/ ==== "
cp main output/
if [ "$?" != 0 ]; then
  exit $?
fi

cp "config/setting.ini.$environment" output/config/setting.ini
if [ "$?" != 0 ]; then
  exit $?
fi

cp "shell/install.sh" output/
if [ "$?" != 0 ]; then
  exit $?
fi

echo "===== 打包重要文件，jenkins归档 ==== "
zip -r output.zip output/
if [ "$?" != 0 ]; then
  exit $?
fi
