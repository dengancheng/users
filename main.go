package main


import (
	"framework/helper/startHelper"
	"users/http"
)

func main() {
	startHelper.ParseArgs()
	startHelper.UseConfig()
	startHelper.UseSeeLog()
	startHelper.UseMongodb()
	startHelper.UseRedis()

	startHelper.StartService(http.HttpService)
}

