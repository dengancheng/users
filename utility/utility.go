package utility

import (
	log "github.com/cihub/seelog"
	"time"
)

func timeCost(start time.Time){
	tc:=time.Since(start)
	log.Debugf("UserLogin TimeCost = %v\n", tc)
}
